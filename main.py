import sys
import time
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
def lines():
    print('---------------')

def main():
    lines()
    print(' EMAIL SPOOFER')
    lines()
    email = input(' Username: ')
    password = input(' Password: ')
    relay = input(' SMTP Server Relay: ')
    port = input(' Port: ')
    from_emails = input(' Sender email: ')
    name = input(' Sender name: ')
    recipient = input(' Recipient email: ')
    lines()

    subject = input(' Email subject: ')
    message = input(' Message:\n > ')

    msg = MIMEMultipart()
    msg['Name'] = name
    msg['From'] = name + '<' + from_emails + '>'
    msg['To'] = recipient
    msg['Subject'] = subject
    msg.attach(MIMEText(message, 'plain'))
    server = smtplib.SMTP(relay, port)
    server.starttls()
    try:
        server.login(email, password)
        text = msg.as_string()
        server.sendmail(from_emails, recipient, text)
        server.quit()
        print('\n')
        print(' Email successfully sent')
        time.sleep(1)
    except Exception as e:
        print('\n')
        print(e)
        time.sleep(0.75)
    
    def resend():
        email_resend = input(' Would you like to send another email? [y/n]\n > ')
        if email_resend == 'y':
            main()
        elif email_resend == 'n':
            time.sleep(0.5)
            leave = input('Press [Enter] to exit...')
            sys.exit()
        else:
            resend()
    resend()
main()